Turrets is a Tower Defense-style game in the making. So far there are five tower types; Electric, Gun, Laser, Fire, and Air.

---------------------------------------

 - [View Applet](http://applets.awesomebox.net/turrets.html)

---------------------------------------

[![Screenshot](https://s3.amazonaws.com/mike-projects/Turrets/Turrets.png)](https://s3.amazonaws.com/mike-projects/Turrets/Turrets.png)