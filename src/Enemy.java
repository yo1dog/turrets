import java.awt.Color;
import java.awt.Graphics;

public class Enemy
{
  public double x;
  public double y;
  public double xv;
  public double yv;
  public final int radius;
  public double speed;
  private final int maxHealth;
  public int health;
  public int ID;
  public boolean dead;
  private int xn;
  private int yn;
  public int currentNode;
  public double pathCompletion;
  private final Color enemyColor;
  private final Path path;
  
  public Enemy(double paramDouble, int paramInt1, int paramInt2, Color paramColor, Path paramPath, int paramInt3)
  {
    this.speed = paramDouble;
    this.radius = paramInt1;
    this.maxHealth = paramInt2;
    this.health = this.maxHealth;
    this.enemyColor = paramColor;
    this.ID = paramInt3;
    
    this.path = paramPath;
    this.currentNode = 1;
    this.pathCompletion = 0.0D;
    this.dead = false;
    
    this.x = this.path.nodes[0][0];
    this.y = this.path.nodes[0][1];
    this.xn = this.path.nodes[1][0];
    this.yn = this.path.nodes[1][1];
    this.xv = paramDouble;
  }
  
  public void kill()
  {
    this.currentNode = 1;
    this.x = this.path.nodes[0][0];
    this.y = this.path.nodes[0][1];
    this.xn = this.path.nodes[1][0];
    this.yn = this.path.nodes[1][1];
  }
  
  public void run()
  {
    double d1 = Math.sqrt((this.xn - this.x) * (this.xn - this.x) + (this.yn - this.y) * (this.yn - this.y));
    if (d1 < this.speed)
    {
      if (this.currentNode < this.path.numNodes - 1)
      {
        this.x = this.xn;
        this.y = this.yn;
        
        this.currentNode += 1;
        this.xn = this.path.nodes[this.currentNode][0];
        this.yn = this.path.nodes[this.currentNode][1];
        
        double d2 = Math.atan2(this.yn - this.y, this.xn - this.x);
        
        this.xv = (Math.cos(d2) * (this.speed - d1));
        this.yv = (Math.sin(d2) * (this.speed - d1));
        
        this.x += this.xv;
        this.y += this.yv;
        
        this.xv = (Math.cos(d2) * this.speed);
        this.yv = (Math.sin(d2) * this.speed);
      }
      else
      {
        kill();
      }
    }
    else
    {
      this.x += this.xv;
      this.y += this.yv;
    }
    double d2 = Math.sqrt(Math.pow(this.xn - this.path.nodes[(this.currentNode - 1)][0], 2.0D) + Math.pow(this.yn - this.path.nodes[(this.currentNode - 1)][1], 2.0D));
    d1 = Math.sqrt((this.xn - this.x) * (this.xn - this.x) + (this.yn - this.y) * (this.yn - this.y));
    
    this.pathCompletion = ((this.currentNode - 1.0D) / (this.path.numNodes - 1) + 1.0D / (this.path.numNodes - 1) * (1.0D - d1 / d2));
  }
  
  public void draw(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.enemyColor);
    paramGraphics.drawOval((int)this.x - this.radius, (int)this.y - this.radius, this.radius * 2, this.radius * 2);
    
    paramGraphics.setColor(Color.red);
    paramGraphics.fillRect((int)(this.x - this.radius / this.maxHealth * this.health), (int)this.y + this.radius + 2, (int)(this.radius / this.maxHealth * this.health * 2.0D), 2);
  }
}
