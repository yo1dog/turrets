import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class AirstrikeTower
{
  private final int x;
  private final int y;
  private final Color towerColor;
  private final Color aimerColor;
  private final Color explotionColor;
  private final Color shadowColor;
  private final Image planeImage;
  private final Color attackRangeColor = new Color(255, 255, 255, 50);
  private final Color backgroundColor;
  private int update;
  private int explotionDamage = 12;
  private int explotionRadius = 30;
  private int fireRate = 100;
  private int explotionUpdate;
  private final int explotionDelay = 50;
  private int eX;
  private int eY;
  private int eR;
  private double aX;
  private double aY;
  private double xv;
  private double yv;
  private double speed;
  private int xn;
  private int yn;
  private int currentNode;
  private final int maxAttackDistance = 100;
  private int attackDistance = 100;
  public Enemy target;
  private boolean first = false;
  private boolean trace = false;
  private final Main main;
  private final Path path;
  
  public AirstrikeTower(int paramInt1, int paramInt2, Color paramColor, Image paramImage, Path paramPath, Main paramMain)
  {
    this.x = paramInt1;
    this.y = paramInt2;
    this.planeImage = paramImage;
    this.path = paramPath;
    this.main = paramMain;
    
    this.backgroundColor = paramColor;
    this.towerColor = Color.gray;
    this.aimerColor = Color.red;
    this.explotionColor = Color.yellow;
    this.shadowColor = new Color(70, 70, 70, 100);
    
    this.update = this.fireRate;
    this.eR = -1;
  }
  
  public void setTarget()
  {
    if (this.trace)
    {
      int i = 0;
      double d2;
      if ((this.target == null) || (this.target.dead == true))
      {
        i = 1;
      }
      else
      {
        d2 = Math.sqrt(Math.pow(this.target.x - this.x, 2.0D) + Math.pow(this.target.y - this.y, 2.0D));
        if ((d2 > this.attackDistance + this.target.radius) || (this.target.health < 1)) {
          i = 1;
        }
      }
      if (i != 0)
      {
        this.target = null;
        int k;
        double d4;
        if (this.first)
        {
          d2 = 0.0D;
          for (k = 0; k < this.main.numEnemys; k++)
          {
            d4 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
            if (d4 < this.attackDistance + this.main.enemy[k].radius) {
              if (this.main.enemy[k].pathCompletion > d2)
              {
                this.target = this.main.enemy[k];
                d2 = this.main.enemy[k].pathCompletion;
              }
            }
          }
        }
        else
        {
          d2 = 1.0D;
          for (k = 0; k < this.main.numEnemys; k++)
          {
            d4 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
            if (d4 < this.attackDistance + this.main.enemy[k].radius) {
              if (this.main.enemy[k].pathCompletion < d2)
              {
                this.target = this.main.enemy[k];
                d2 = this.main.enemy[k].pathCompletion;
              }
            }
          }
        }
      }
    }
    else
    {
      this.target = null;
      double d1;
      int j;
      double d3;
      if (this.first)
      {
        d1 = 0.0D;
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d3 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d3 < this.attackDistance + this.main.enemy[j].radius) {
            if (this.main.enemy[j].pathCompletion > d1)
            {
              this.target = this.main.enemy[j];
              d1 = this.main.enemy[j].pathCompletion;
            }
          }
        }
      }
      else
      {
        d1 = 1.0D;
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d3 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d3 < this.attackDistance + this.main.enemy[j].radius) {
            if (this.main.enemy[j].pathCompletion < d1)
            {
              this.target = this.main.enemy[j];
              d1 = this.main.enemy[j].pathCompletion;
            }
          }
        }
      }
    }
  }
  
  private void setAimer()
  {
    this.aX = this.target.x;
    this.aY = this.target.y;
    this.speed = (this.target.speed * 50.0D);
    this.currentNode = this.target.currentNode;
    this.xn = this.path.nodes[this.currentNode][0];
    this.yn = this.path.nodes[this.currentNode][1];
    
    runAimer();
  }
  
  private void createExplotion()
  {
    this.eX = ((int)this.aX);
    this.eY = ((int)this.aY);
    
    this.eR = 0;
    this.explotionUpdate = 0;
  }
  
  private boolean explotionHit(int paramInt)
  {
    this.main.enemy[paramInt].health -= this.explotionDamage;
    if (this.main.enemy[paramInt].health < 1)
    {
      this.main.killEnemy(paramInt);
      return true;
    }
    return false;
  }
  
  private void attack() {}
  
  public void run()
  {
    if (this.update == this.fireRate)
    {
      setTarget();
      if (this.target != null)
      {
        setAimer();
        this.update += 1;
      }
    }
    else
    {
      this.update += 1;
    }
    if (this.update == this.fireRate + 50)
    {
      createExplotion();
      this.update = 0;
    }
    runExplotion();
  }
  
  private void runAimer()
  {
    double d1 = Math.sqrt((this.xn - this.aX) * (this.xn - this.aX) + (this.yn - this.aY) * (this.yn - this.aY));
    double d2;
    if (d1 < this.speed)
    {
      if (this.currentNode < this.path.numNodes - 1)
      {
        this.aX = this.xn;
        this.aY = this.yn;
        
        this.currentNode += 1;
        this.xn = this.path.nodes[this.currentNode][0];
        this.yn = this.path.nodes[this.currentNode][1];
        
        d2 = Math.atan2(this.yn - this.aY, this.xn - this.aX);
        
        this.xv = (Math.cos(d2) * (this.speed - d1));
        this.yv = (Math.sin(d2) * (this.speed - d1));
        
        this.aX += this.xv;
        this.aY += this.yv;
      }
      else
      {
        this.aX = this.xn;
        this.aY = this.yn;
      }
    }
    else
    {
      d2 = Math.atan2(this.yn - this.aY, this.xn - this.aX);
      
      this.xv = (Math.cos(d2) * this.speed);
      this.yv = (Math.sin(d2) * this.speed);
      
      this.aX += this.xv;
      this.aY += this.yv;
    }
  }
  
  private void runExplotion()
  {
    if (this.eR > -1)
    {
      this.explotionUpdate += 5;
      this.eR = (10 + this.explotionRadius - Math.abs(-this.explotionRadius + this.explotionUpdate + 10));
      
      int i = 0;
      int[] arrayOfInt = new int[this.main.numEnemys];
      for (int j = 0; j < this.main.numEnemys; j++)
      {
        double d = Math.sqrt(Math.pow(this.main.enemy[j].x - this.eX, 2.0D) + Math.pow(this.main.enemy[j].y - this.eY, 2.0D));
        if (d < this.explotionRadius + this.main.enemy[j].radius)
        {
          arrayOfInt[i] = j;
          i++;
        }
      }
      int j = 0;
      for (int k = 0; j < i; j++) {
        if (explotionHit(arrayOfInt[j] - k)) {
          k++;
        }
      }
    }
  }
  
  public void drawAttackRange(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.attackRangeColor);
    paramGraphics.fillOval(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2);
  }
  
  public void draw(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.towerColor);
    paramGraphics.drawArc(this.x - 6, this.y - 15, 25, 30, 100, 160);
    paramGraphics.fillOval(this.x + 5, this.y - 2, 4, 4);
    paramGraphics.drawLine(this.x - 3, this.y - 10, this.x + 7, this.y);
    paramGraphics.drawLine(this.x - 3, this.y + 9, this.x + 6, this.y);
    paramGraphics.fillRect(this.x - 6, this.y - 1, 6, 3);
    

    drawExplotion(paramGraphics);
    if (this.update > this.fireRate)
    {
      paramGraphics.setColor(this.aimerColor);
      paramGraphics.drawOval((int)this.target.x - 6, (int)this.target.y - 6, 12, 12);
      paramGraphics.drawLine((int)this.target.x, (int)this.target.y - 8, (int)this.target.x, (int)this.target.y + 8);
      paramGraphics.drawLine((int)this.target.x - 8, (int)this.target.y, (int)this.target.x + 8, (int)this.target.y);
      paramGraphics.fillOval(this.x + 5, this.y - 2, 4, 4);
      if (this.update > this.fireRate + 15)
      {
        double d = 1.0D - (this.update - 15 - this.fireRate) / 35.0D;
        
        int i = 4 + (int)(d * 10.0D);
        paramGraphics.fillArc((int)(this.aX - d * 160.0D) - i, (int)(this.aY - d * 6.0D) - i, i * 4, i, 270, 180);
      }
      double d = 1.0D - (this.update - this.fireRate) / 50.0D;
      
      paramGraphics.drawImage(this.planeImage, (int)(this.aX + 400.0D - d * 800.0D + 5.0D), (int)this.aY - 38, 70, 47, null);
    }
  }
  
  private void drawExplotion(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.explotionColor);
    paramGraphics.fillOval(this.eX - this.eR, this.eY - this.eR, this.eR * 2, this.eR * 2);
  }
}