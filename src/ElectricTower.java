import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MultipleGradientPaint;
import java.awt.RadialGradientPaint;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Float;

public class ElectricTower
{
  private final int x;
  private final int y;
  private final byte zapperRadius = 8;
  private final Color towerColor;
  private final Color electricColor;
  private final Color electricMidColor;
  private final Color electricFadeColor;
  private final int dRed;
  private final int oRed;
  private final int dGreen;
  private final int oGreen;
  private final int dBlue;
  private final int oBlue;
  private final Color attackRangeColor = new Color(255, 255, 255, 50);
  private byte update;
  private final RadialGradientPaint zapperGradient;
  private final Ellipse2D zapperShape;
  private final int segmentLength = 8;
  private final int maxRange = 6;
  private final int maxRangeChnage = 5;
  private final int updateRate = 1;
  private final int maxAttackDistance = 100;
  private int attackDistance;
  private final int maxNumTargets = 3;
  private int numTargets;
  private int attackDamage = 1;
  private boolean first = true;
  private boolean trace = true;
  private int[] numSeg;
  private int[][] segments = new int[3][14];
  public Enemy[] target = new Enemy[3];
  private Main main;
  
  public ElectricTower(int paramInt1, int paramInt2, Main paramMain)
  {
    this.x = paramInt1;
    this.y = paramInt2;
    this.towerColor = Color.gray;
    this.electricColor = Color.white;
    this.electricMidColor = Color.yellow;
    
    this.oRed = this.electricColor.getRed();
    this.oGreen = this.electricColor.getGreen();
    this.oBlue = this.electricColor.getBlue();
    
    this.dRed = (this.electricMidColor.getRed() - this.oRed);
    this.dGreen = (this.electricMidColor.getGreen() - this.oGreen);
    this.dBlue = (this.electricMidColor.getBlue() - this.oBlue);
    
    this.electricFadeColor = new Color(this.electricColor.getRed(), this.electricColor.getGreen(), this.electricColor.getBlue(), 100);
    
    this.main = paramMain;
    
    this.attackDistance = 100;
    this.numTargets = 3;
    
    this.update = 0;
    
    float[] arrayOfFloat = { 0.0F, 1.0F };
    Color[] arrayOfColor = { new Color(240, 240, 240), new Color(100, 100, 100) };
    this.zapperGradient = new RadialGradientPaint(paramInt1, paramInt2, 8.0F, paramInt1 - 4, paramInt2 - 4, arrayOfFloat, arrayOfColor, MultipleGradientPaint.CycleMethod.NO_CYCLE);
    
    this.zapperShape = new Ellipse2D.Float(paramInt1 - 8, paramInt2 - 8, 16.0F, 16.0F);
  }
  
  private void updateElectric()
  {
    this.numSeg = new int[this.numTargets];
    for (int i = 0; i < this.numTargets; i++) {
      if (this.target[i] != null)
      {
        double d = Math.sqrt(Math.pow(this.target[i].x - this.x, 2.0D) + Math.pow(this.target[i].y - this.y, 2.0D));
        this.numSeg[i] = ((int)(d / 8.0D));
        for (int j = 0; j < this.numSeg[i] - 1; j++) {
          this.segments[i][j] = ((int)(Math.random() * 12.0D - 6.0D));
        }
      }
    }
  }
  
  public void setTarget()
  {
    int j;
    double d1;
    int k;
    if (this.trace)
    {
      for (int i = 0; i < this.numTargets; i++)
      {
        j = 0;
        if ((this.target[i] == null) || (this.target[i].dead == true))
        {
          j = 1;
        }
        else
        {
          d1 = Math.sqrt(Math.pow(this.target[i].x - this.x, 2.0D) + Math.pow(this.target[i].y - this.y, 2.0D));
          if ((d1 > this.attackDistance + this.target[i].radius) || (this.target[i].health < 1)) {
            j = 1;
          }
        }
        if (j != 0)
        {
          this.target[i] = null;
          double d2;
          int n;
          int i1;
          if (this.first)
          {
            d1 = 0.0D;
            for (k = 0; k < this.main.numEnemys; k++)
            {
              d2 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
              if (d2 < this.attackDistance + this.main.enemy[k].radius) {
                if (this.main.enemy[k].pathCompletion > d1)
                {
                  n = 1;
                  for (i1 = 0; i1 < this.numTargets; i1++) {
                    if (this.target[i1] == this.main.enemy[k])
                    {
                      n = 0;
                      break;
                    }
                  }
                  if (n != 0)
                  {
                    this.target[i] = this.main.enemy[k];
                    d1 = this.main.enemy[k].pathCompletion;
                  }
                }
              }
            }
          }
          else
          {
            d1 = 1.0D;
            for (k = 0; k < this.main.numEnemys; k++)
            {
              d2 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
              if (d2 < this.attackDistance + this.main.enemy[k].radius) {
                if (this.main.enemy[k].pathCompletion < d1)
                {
                  n = 1;
                  for (i1 = 0; i1 < this.numTargets; i1++) {
                    if (this.target[i1] == this.main.enemy[k])
                    {
                      n = 0;
                      break;
                    }
                  }
                  if (n != 0)
                  {
                    this.target[i] = this.main.enemy[k];
                    d1 = this.main.enemy[k].pathCompletion;
                  }
                }
              }
            }
          }
        }
      }
    }
    else
    {
      double[] arrayOfDouble;
      int m;
      if (this.first)
      {
        arrayOfDouble = new double[this.numTargets];
        this.target = new Enemy[3];
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d1 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d1 < this.attackDistance + this.main.enemy[j].radius) {
            for (k = 0; k < this.numTargets; k++) {
              if (this.main.enemy[j].pathCompletion > arrayOfDouble[k])
              {
                for (m = 1; m < this.numTargets - k; m++)
                {
                  arrayOfDouble[(this.numTargets - m)] = arrayOfDouble[(this.numTargets - m - 1)];
                  
                  this.target[(this.numTargets - m)] = this.target[(this.numTargets - m - 1)];
                }
                arrayOfDouble[k] = this.main.enemy[j].pathCompletion;
                
                this.target[k] = this.main.enemy[j];
                
                break;
              }
            }
          }
        }
      }
      else
      {
        arrayOfDouble = new double[this.numTargets];
        this.target = new Enemy[3];
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d1 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d1 < this.attackDistance + this.main.enemy[j].radius) {
            for (k = 0; k < this.numTargets; k++) {
              if ((this.main.enemy[j].pathCompletion < arrayOfDouble[k]) || (arrayOfDouble[k] == 0.0D))
              {
                for (m = 1; m < this.numTargets - k; m++)
                {
                  arrayOfDouble[(this.numTargets - m)] = arrayOfDouble[(this.numTargets - m - 1)];
                  
                  this.target[(this.numTargets - m)] = this.target[(this.numTargets - m - 1)];
                }
                arrayOfDouble[k] = this.main.enemy[j].pathCompletion;
                
                this.target[k] = this.main.enemy[j];
                
                break;
              }
            }
          }
        }
      }
    }
  }
  
  private void attack()
  {
    for (int i = 0; i < this.numTargets; i++) {
      if (this.target[i] != null)
      {
        this.target[i].health -= this.attackDamage;
        if (this.target[i].health < 1)
        {
          this.main.killEnemy(this.target[i].ID);
          this.target[i] = null;
        }
      }
    }
  }
  
  public void run()
  {
    setTarget();
    attack();
    
    this.update = ((byte)(this.update + 1));
    if (this.update == 1)
    {
      updateElectric();
      this.update = 0;
    }
  }
  
  public void drawAttackRange(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.attackRangeColor);
    paramGraphics.fillOval(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2);
  }
  
  public void draw(Graphics paramGraphics, Graphics2D paramGraphics2D)
  {
    drawTower(paramGraphics, paramGraphics2D);
    drawAttack(paramGraphics);
  }
  
  public void drawTower(Graphics paramGraphics, Graphics2D paramGraphics2D)
  {
    paramGraphics.setColor(this.towerColor);
    paramGraphics.drawRect(this.x - 15, this.y - 15, 30, 30);
    
    paramGraphics2D.setPaint(this.zapperGradient);
    paramGraphics2D.fill(this.zapperShape);
  }
  
  private void drawAttack(Graphics paramGraphics)
  {
    for (int i = 0; i < this.numTargets; i++) {
      if (this.target[i] != null)
      {
        double d1 = Math.atan2(this.target[i].y - this.y, this.target[i].x - this.x);
        
        int j = this.x + (int)(Math.cos(d1) * 8.0D);
        int k = this.y + (int)(Math.sin(d1) * 8.0D);
        int m = (int)(this.target[i].x - Math.cos(d1) * this.target[i].radius);
        int n = (int)(this.target[i].y - Math.sin(d1) * this.target[i].radius);
        
        double d2 = Math.sqrt((m - j) * (m - j) + (n - k) * (n - k));
        
        double d3 = d2 - this.numSeg[i] * 8;
        if (d3 < 0.0D)
        {
          this.numSeg[i] -= (int)(-d3 / 8.0D);
          d3 = (d2 - this.numSeg[i] * 8) / 2.0D;
        }
        int i1 = j;
        int i2 = k;
        int i3 = j;
        int i4 = k;
        for (int i5 = 0; i5 < this.numSeg[i] - 1; i5++)
        {
          double d4 = j + Math.cos(d1) * (8 * (i5 + 1) + d3 / this.numSeg[i] * (i5 + 1));
          double d5 = k + Math.sin(d1) * (8 * (i5 + 1) + d3 / this.numSeg[i] * (i5 + 1));
          
          double d6 = this.segments[i][i5];
          
          i3 = (int)(d4 + Math.cos(d1 + 1.570796326794897D) * d6);
          i4 = (int)(d5 + Math.sin(d1 + 1.570796326794897D) * d6);
          

          int i6 = this.oRed + (this.dRed > 0 ? 2 : -2) * (int)(Math.abs(this.dRed * 0.5D) - Math.abs(-(1.0D / (this.numSeg[i] - 1)) * this.dRed * i5 + this.dRed * 0.5D));
          int i7 = this.oGreen + (this.dGreen > 0 ? 2 : -2) * (int)(Math.abs(this.dGreen * 0.5D) - Math.abs(-(1.0D / (this.numSeg[i] - 1)) * this.dGreen * i5 + this.dGreen * 0.5D));
          int i8 = this.oBlue + (this.dBlue > 0 ? 2 : -2) * (int)(Math.abs(this.dBlue * 0.5D) - Math.abs(-(1.0D / (this.numSeg[i] - 1)) * this.dBlue * i5 + this.dBlue * 0.5D));
          
          paramGraphics.setColor(new Color(i6, i7, i8));
          paramGraphics.drawLine(i1, i2, i3, i4);
          
          i1 = i3;
          i2 = i4;
        }
        paramGraphics.setColor(this.electricColor);
        paramGraphics.drawLine(i1, i2, m, n);
        
        paramGraphics.setColor(this.electricFadeColor);
        paramGraphics.fillOval(j - 2, k - 2, 4, 4);
        paramGraphics.fillOval(m - 2, n - 2, 4, 4);
      }
    }
  }
}
