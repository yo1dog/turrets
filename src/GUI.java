import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class GUI
{
  private final Color backgroundColor = new Color(40, 40, 40);
  private final Color borderColor = Color.green;
  private final Color boxColor = new Color(60, 60, 0);
  private final Color boxSelectedColor = Color.yellow;
  private final Color fontColor = Color.white;
  private final int width;
  private final int height;
  private final byte boxOffsetX = 2;
  private final byte boxOffsetY = 2;
  private final byte numBoxes = 5;
  private byte selected;
  private final Main main;
  
  public GUI(int paramInt1, int paramInt2, Main paramMain)
  {
    this.width = (paramInt1 - 1);
    this.height = paramInt2;
    this.main = paramMain;
    this.selected = 0;
  }
  
  public void mouseDown(int paramInt1, int paramInt2)
  {
    if (paramInt2 < this.height) {
      this.main.addTower(paramInt1, paramInt2, this.selected);
    } else if ((paramInt1 > 2) && (paramInt1 < 182) && (paramInt2 > this.height + 2) && (paramInt2 < this.height + 2 + 36)) {
      this.selected = ((byte)((paramInt1 - 2) / 36));
    }
  }
  
  public void keyDown(int paramInt)
  {
    if ((paramInt > 47) && (paramInt < 59)) {
      this.selected = ((byte)(paramInt - 49));
    }
  }
  
  public void draw(Graphics paramGraphics, Graphics2D paramGraphics2D)
  {
    paramGraphics.setColor(this.backgroundColor);
    paramGraphics.fillRect(0, this.height, this.width, 100);
    
    paramGraphics.setColor(this.borderColor);
    paramGraphics.drawRect(0, this.height, this.width, 100);
    

    paramGraphics.setColor(this.boxColor);
    for (int i = 0; i < 5; i++) {
      paramGraphics.drawRect(2 + i * 36, this.height + 2, 36, 36);
    }
    paramGraphics.setColor(this.boxSelectedColor);
    paramGraphics.drawRect(2 + this.selected * 36, this.height + 2, 36, 36);
    


    paramGraphics.setColor(this.fontColor);
    paramGraphics.drawString("Elec", 4, this.height + 2 + 12);
    paramGraphics.drawString("Gun", 40, this.height + 2 + 16);
    paramGraphics.drawString("Laser", 76, this.height + 2 + 20);
    paramGraphics.drawString("Fire", 112, this.height + 2 + 24);
    paramGraphics.drawString("Air", 148, this.height + 2 + 28);
    


    paramGraphics.drawString("Click on the boxes above or use keys 1 - 5 to select tower type", 2, 455);
    paramGraphics.drawString("Click anywhere in the room to place a tower", 2, 469);
    paramGraphics.drawString("Press the space bar to stop and start", 2, 483);
  }
}
