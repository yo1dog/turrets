import java.awt.Color;
import java.awt.Graphics;

public class Path
{
  private final int trackWidth;
  public final int numNodes;
  private final Color trackColor;
  public final int[][] nodes;
  private final int[][] trackNodes;
  
  public Path(int paramInt1, int[][] paramArrayOfInt, int paramInt2, Color paramColor)
  {
    this.numNodes = paramInt1;
    this.trackWidth = paramInt2;
    this.trackColor = paramColor;
    this.nodes = paramArrayOfInt;
    

    this.trackNodes = new int[this.numNodes][4];
    for (int i = 1; i < this.numNodes - 1; i++)
    {
      double d2 = Math.atan2(paramArrayOfInt[i][1] - paramArrayOfInt[(i - 1)][1], paramArrayOfInt[i][0] - paramArrayOfInt[(i - 1)][0]);
      double d4 = Math.atan2(paramArrayOfInt[(i + 1)][1] - paramArrayOfInt[i][1], paramArrayOfInt[(i + 1)][0] - paramArrayOfInt[i][0]);
      
      double d6 = (d2 + d4) / 2.0D + 1.570796326794897D;
      
      double d8 = this.trackWidth / 2;
      double d9 = d8 / Math.tan(d6);
      
      double d10 = Math.sqrt(d8 * d8 + d9 * d9);
      
      int i1 = (int)(this.nodes[i][0] + Math.cos(d6) * d10);
      int i2 = (int)(this.nodes[i][1] + Math.sin(d6) * d10);
      int i3 = (int)(this.nodes[i][0] - Math.cos(d6) * d10);
      int i4 = (int)(this.nodes[i][1] - Math.sin(d6) * d10);
      
      this.trackNodes[i][0] = i1;
      this.trackNodes[i][1] = i2;
      this.trackNodes[i][2] = i3;
      this.trackNodes[i][3] = i4;
    }
    double d1 = Math.atan2(paramArrayOfInt[1][1] - paramArrayOfInt[0][1], paramArrayOfInt[1][0] - paramArrayOfInt[0][0]) + 1.570796326794897D;
    
    double d3 = this.trackWidth / 2;
    double d5 = d3 / Math.tan(d1);
    
    double d7 = Math.sqrt(d3 * d3 + d5 * d5);
    
    int j = (int)(this.nodes[0][0] + Math.cos(d1) * d7);
    int k = (int)(this.nodes[0][1] + Math.sin(d1) * d7);
    int m = (int)(this.nodes[0][0] - Math.cos(d1) * d7);
    int n = (int)(this.nodes[0][1] - Math.sin(d1) * d7);
    
    this.trackNodes[0][0] = j;
    this.trackNodes[0][1] = k;
    this.trackNodes[0][2] = m;
    this.trackNodes[0][3] = n;
    

    d1 = Math.atan2(paramArrayOfInt[(this.numNodes - 1)][1] - paramArrayOfInt[(this.numNodes - 2)][1], paramArrayOfInt[(this.numNodes - 1)][0] - paramArrayOfInt[(this.numNodes - 2)][0]) + 1.570796326794897D;
    
    d3 = this.trackWidth / 2;
    d5 = d3 / Math.tan(d1);
    
    d7 = Math.sqrt(d3 * d3 + d5 * d5);
    
    j = (int)(this.nodes[(this.numNodes - 1)][0] + Math.cos(d1) * d7);
    k = (int)(this.nodes[(this.numNodes - 1)][1] + Math.sin(d1) * d7);
    m = (int)(this.nodes[(this.numNodes - 1)][0] - Math.cos(d1) * d7);
    n = (int)(this.nodes[(this.numNodes - 1)][1] - Math.sin(d1) * d7);
    
    this.trackNodes[(this.numNodes - 1)][0] = j;
    this.trackNodes[(this.numNodes - 1)][1] = k;
    this.trackNodes[(this.numNodes - 1)][2] = m;
    this.trackNodes[(this.numNodes - 1)][3] = n;
  }
  
  public void draw(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.trackColor);
    for (int i = 1; i < this.numNodes; i++)
    {
      paramGraphics.drawLine(this.trackNodes[(i - 1)][0], this.trackNodes[(i - 1)][1], this.trackNodes[i][0], this.trackNodes[i][1]);
      paramGraphics.drawLine(this.trackNodes[(i - 1)][2], this.trackNodes[(i - 1)][3], this.trackNodes[i][2], this.trackNodes[i][3]);
    }
  }
}
