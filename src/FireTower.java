import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class FireTower
{
  private final int x;
  private final int y;
  private final Color towerColor;
  private final Color gunColor;
  private Color fireColor;
  private final Color attackRangeColor = new Color(255, 255, 255, 50);
  private final Polygon bunkerShape;
  private int update;
  private int particleTime;
  private final double fireSpeed = 4.0D;
  private int fireDamage = 1;
  private final int maxFireRate = 1;
  private int fireRate = 1;
  private boolean[] bunkerEnabled;
  private int[] numFireParticles;
  private double[][][] fireParticles;
  private final int maxAttackDistance = 70;
  private int attackDistance = 70;
  private double maxSpred = 0.4487989505128276D;
  private final Main main;
  
  public FireTower(int paramInt1, int paramInt2, Main paramMain)
  {
    this.x = paramInt1;
    this.y = paramInt2;
    this.main = paramMain;
    
    this.bunkerEnabled = new boolean[8];
    this.numFireParticles = new int[8];
    this.fireParticles = new double[8][50][5];
    for (int i = 0; i < 8; i++) {
      this.numFireParticles[i] = 0;
    }
    this.bunkerEnabled[0] = true;
    this.bunkerEnabled[1] = true;
    this.bunkerEnabled[2] = true;
    this.bunkerEnabled[3] = true;
    this.bunkerEnabled[4] = true;
    this.bunkerEnabled[5] = true;
    this.bunkerEnabled[6] = true;
    this.bunkerEnabled[7] = true;
    
    this.towerColor = new Color(168, 90, 49);
    this.gunColor = Color.red;
    this.fireColor = new Color(255, 127, 0, 128);
    this.update = 0;
    this.particleTime = ((int)(this.attackDistance / 3.0D));
    
    this.bunkerShape = new Polygon();
    this.bunkerShape.addPoint(paramInt1 + 15, paramInt2 + 5);
    this.bunkerShape.addPoint(paramInt1 + 15, paramInt2 - 5);
    this.bunkerShape.addPoint(paramInt1 + 5, paramInt2 - 15);
    this.bunkerShape.addPoint(paramInt1 - 5, paramInt2 - 15);
    this.bunkerShape.addPoint(paramInt1 - 15, paramInt2 - 5);
    this.bunkerShape.addPoint(paramInt1 - 15, paramInt2 + 5);
    this.bunkerShape.addPoint(paramInt1 - 5, paramInt2 + 15);
    this.bunkerShape.addPoint(paramInt1 + 5, paramInt2 + 15);
  }
  
  private void createFire(int paramInt)
  {
    this.fireParticles[paramInt][this.numFireParticles[paramInt]][0] = this.x;
    this.fireParticles[paramInt][this.numFireParticles[paramInt]][1] = this.y;
    double d = 0.7853981633974483D * paramInt + Math.random() * this.maxSpred - this.maxSpred * 0.5D;
    this.fireParticles[paramInt][this.numFireParticles[paramInt]][2] = (Math.cos(d) * 4.0D);
    this.fireParticles[paramInt][this.numFireParticles[paramInt]][3] = (Math.sin(d) * 4.0D);
    this.fireParticles[paramInt][this.numFireParticles[paramInt]][4] = 0.0D;
    
    this.numFireParticles[paramInt] += 1;
  }
  
  private void killFire(int paramInt)
  {
    this.numFireParticles[paramInt] -= 1;
    for (int i = 0; i < this.numFireParticles[paramInt]; i++)
    {
      this.fireParticles[paramInt][i][0] = this.fireParticles[paramInt][(i + 1)][0];
      this.fireParticles[paramInt][i][1] = this.fireParticles[paramInt][(i + 1)][1];
      this.fireParticles[paramInt][i][2] = this.fireParticles[paramInt][(i + 1)][2];
      this.fireParticles[paramInt][i][3] = this.fireParticles[paramInt][(i + 1)][3];
      this.fireParticles[paramInt][i][4] = this.fireParticles[paramInt][(i + 1)][4];
    }
  }
  
  private boolean fireHit(int paramInt)
  {
    this.main.enemy[paramInt].health -= this.fireDamage;
    if (this.main.enemy[paramInt].health < 1)
    {
      this.main.killEnemy(paramInt);
      return true;
    }
    return false;
  }
  
  private void attack()
  {
    this.update += 1;
    if (this.update == this.fireRate)
    {
      int i = 0;
      int j = 0;
      int k = 0;
      int m = 0;
      int n = 0;
      int i1 = 0;
      int i2 = 0;
      int i3 = 0;
      
      int i4 = 0;
      int[] arrayOfInt = new int[this.main.numEnemys];
      for (int i5 = 0; i5 < this.main.numEnemys; i5++)
      {
        double d1 = Math.sqrt(Math.pow(this.x - this.main.enemy[i5].x, 2.0D) + Math.pow(this.y - this.main.enemy[i5].y, 2.0D));
        if (d1 < this.attackDistance + this.main.enemy[i5].radius)
        {
          int i7 = 1;
          double d2 = Math.atan2(this.y - this.main.enemy[i5].y, this.x - this.main.enemy[i5].x);
          if (this.bunkerEnabled[0]) {
            if ((d2 > 3.141592653589793D - this.maxSpred) || (d2 < -3.141592653589793D + this.maxSpred))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              i = 1;
            }
          }
          if (this.bunkerEnabled[1]) {
            if ((d2 > -2.804993440705172D) && (d2 < -1.907395539679518D))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              j = 1;
            }
          }
          if (this.bunkerEnabled[2]) {
            if ((d2 > -1.570796326794897D - this.maxSpred) && (d2 < -1.570796326794897D + this.maxSpred))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              k = 1;
            }
          }
          if (this.bunkerEnabled[3]) {
            if ((d2 > -0.7853981633974483D - this.maxSpred) && (d2 < -0.7853981633974483D + this.maxSpred))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              m = 1;
            }
          }
          if (this.bunkerEnabled[4]) {
            if ((d2 > -this.maxSpred) && (d2 < this.maxSpred))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              n = 1;
            }
          }
          if (this.bunkerEnabled[5]) {
            if ((d2 > 0.7853981633974483D - this.maxSpred) && (d2 < 0.7853981633974483D + this.maxSpred))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              i1 = 1;
            }
          }
          if (this.bunkerEnabled[6]) {
            if ((d2 > 1.570796326794897D - this.maxSpred) && (d2 < 1.570796326794897D + this.maxSpred))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              i2 = 1;
            }
          }
          if (this.bunkerEnabled[7]) {
            if ((d2 > 2.356194490192345D - this.maxSpred) && (d2 < 2.356194490192345D + this.maxSpred))
            {
              if (i7 != 0)
              {
                arrayOfInt[i4] = i5;
                i4++;
                
                i7 = 0;
              }
              i3 = 1;
            }
          }
        }
      }
      int i5 = 0;
      for (int i6 = 0; i5 < i4; i5++) {
        if (fireHit(arrayOfInt[i5] - i6)) {
          i6++;
        }
      }
      if (i != 0) {
        createFire(0);
      }
      if (j != 0) {
        createFire(1);
      }
      if (k != 0) {
        createFire(2);
      }
      if (m != 0) {
        createFire(3);
      }
      if (n != 0) {
        createFire(4);
      }
      if (i1 != 0) {
        createFire(5);
      }
      if (i2 != 0) {
        createFire(6);
      }
      if (i3 != 0) {
        createFire(7);
      }
      this.update = 0;
    }
  }
  
  public void run()
  {
    attack();
    
    runFire();
  }
  
  private void runFire()
  {
    for (int i = 0; i < 8; i++) {
      if (this.bunkerEnabled[i]) {
        for (int j = 0; j < this.numFireParticles[i]; j++)
        {
          this.fireParticles[i][j][0] += this.fireParticles[i][j][2];
          this.fireParticles[i][j][1] += this.fireParticles[i][j][3];
          this.fireParticles[i][j][4] += 1.0D;
          if (this.fireParticles[i][j][4] > this.particleTime) {
            killFire(i);
          }
        }
      }
    }
  }
  
  public void drawAttackRange(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.attackRangeColor);
    if (this.bunkerEnabled[0]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 337, 45);
    }
    if (this.bunkerEnabled[1]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 292, 45);
    }
    if (this.bunkerEnabled[2]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 247, 45);
    }
    if (this.bunkerEnabled[3]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 202, 45);
    }
    if (this.bunkerEnabled[4]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 157, 45);
    }
    if (this.bunkerEnabled[5]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 112, 45);
    }
    if (this.bunkerEnabled[6]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 67, 45);
    }
    if (this.bunkerEnabled[7]) {
      paramGraphics.fillArc(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2, 22, 45);
    }
  }
  
  public void draw(Graphics paramGraphics)
  {
    drawFire(paramGraphics);
    
    paramGraphics.setColor(this.towerColor);
    paramGraphics.fillPolygon(this.bunkerShape);
    
    paramGraphics.setColor(Color.black);
    if (this.bunkerEnabled[0]) {
      paramGraphics.drawLine(this.x + 8, this.y, this.x + 12, this.y);
    }
    if (this.bunkerEnabled[1]) {
      paramGraphics.drawLine(this.x + 5, this.y + 5, this.x + 8, this.y + 8);
    }
    if (this.bunkerEnabled[2]) {
      paramGraphics.drawLine(this.x, this.y + 8, this.x, this.y + 12);
    }
    if (this.bunkerEnabled[3]) {
      paramGraphics.drawLine(this.x - 5, this.y + 5, this.x - 8, this.y + 8);
    }
    if (this.bunkerEnabled[4]) {
      paramGraphics.drawLine(this.x - 8, this.y, this.x - 12, this.y);
    }
    if (this.bunkerEnabled[5]) {
      paramGraphics.drawLine(this.x - 5, this.y - 5, this.x - 8, this.y - 8);
    }
    if (this.bunkerEnabled[6]) {
      paramGraphics.drawLine(this.x, this.y - 8, this.x, this.y - 12);
    }
    if (this.bunkerEnabled[7]) {
      paramGraphics.drawLine(this.x + 5, this.y - 5, this.x + 8, this.y - 8);
    }
  }
  
  private void drawFire(Graphics paramGraphics)
  {
    for (int i = 0; i < 8; i++) {
      if (this.bunkerEnabled[i]) {
        for (int j = 0; j < this.numFireParticles[i]; j++)
        {
          paramGraphics.setColor(new Color(255, (int)(255.0D - this.fireParticles[i][j][4] * 255.0D / this.particleTime), 0, 200 - (int)(this.fireParticles[i][j][4] * this.fireParticles[i][j][4] / (this.particleTime * this.particleTime) * 200.0D)));
          int k = 4 + (int)(this.fireParticles[i][j][4] * 0.4D);
          paramGraphics.fillOval((int)this.fireParticles[i][j][0] - k, (int)this.fireParticles[i][j][1] - k, k * 2, k * 2);
        }
      }
    }
  }
}
