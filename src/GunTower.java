import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class GunTower
{
  private final int width;
  private final int height;
  private final int x;
  private final int y;
  private final Color towerColor;
  private Color bulletColor;
  private final Color gunColor;
  private final Color attackRangeColor = new Color(255, 255, 255, 50);
  private double angle;
  private int update;
  private final int maxSpinSpeed = 45;
  private int spinSpeed;
  private int spin;
  private final int spinFriction = 1;
  private final int bulletSpeed = 20;
  private int bulletDamage = 8;
  private final double bulletAccuracy = 0.174532925199433D;
  private final int maxFireRate = 3;
  private final int randomFireRate = 3;
  private int fireRate = 3;
  private int numBullets;
  private final int maxBulletDistance;
  private double[][] bullet;
  private final int maxAttackDistance = 150;
  private int attackDistance = 150;
  public Enemy target;
  private boolean first = true;
  private boolean trace = false;
  private double xx1;
  private double yy1;
  private double xx2;
  private double yy2;
  private final Main main;
  
  public GunTower(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Main paramMain)
  {
    this.x = paramInt1;
    this.y = paramInt2;
    this.width = paramInt3;
    this.height = paramInt4;
    this.main = paramMain;
    
    this.angle = 0.0D;
    this.spin = 0;
    this.spinSpeed = 0;
    
    this.maxBulletDistance = ((int)Math.sqrt(this.width * this.width + this.height * this.height));
    this.bullet = new double[(int)(this.maxBulletDistance / 20.0D / 3.0D) + 3][4];
    
    this.towerColor = Color.gray;
    this.gunColor = Color.green;
    this.bulletColor = Color.yellow;
    this.numBullets = 0;
    this.update = 0;
  }
  
  public void setTarget()
  {
    if (this.trace)
    {
      int i = 0;
      double d2;
      if ((this.target == null) || (this.target.dead == true))
      {
        i = 1;
      }
      else
      {
        d2 = Math.sqrt(Math.pow(this.target.x - this.x, 2.0D) + Math.pow(this.target.y - this.y, 2.0D));
        if ((d2 > this.attackDistance + this.target.radius) || (this.target.health < 1)) {
          i = 1;
        }
      }
      if (i != 0)
      {
        this.target = null;
        int k;
        double d4;
        if (this.first)
        {
          d2 = 0.0D;
          for (k = 0; k < this.main.numEnemys; k++)
          {
            d4 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
            if (d4 < this.attackDistance + this.main.enemy[k].radius) {
              if (this.main.enemy[k].pathCompletion > d2)
              {
                this.target = this.main.enemy[k];
                d2 = this.main.enemy[k].pathCompletion;
              }
            }
          }
        }
        else
        {
          d2 = 1.0D;
          for (k = 0; k < this.main.numEnemys; k++)
          {
            d4 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
            if (d4 < this.attackDistance + this.main.enemy[k].radius) {
              if (this.main.enemy[k].pathCompletion < d2)
              {
                this.target = this.main.enemy[k];
                d2 = this.main.enemy[k].pathCompletion;
              }
            }
          }
        }
      }
    }
    else
    {
      this.target = null;
      double d1;
      int j;
      double d3;
      if (this.first)
      {
        d1 = 0.0D;
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d3 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d3 < this.attackDistance + this.main.enemy[j].radius) {
            if (this.main.enemy[j].pathCompletion > d1)
            {
              this.target = this.main.enemy[j];
              d1 = this.main.enemy[j].pathCompletion;
            }
          }
        }
      }
      else
      {
        d1 = 1.0D;
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d3 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d3 < this.attackDistance + this.main.enemy[j].radius) {
            if (this.main.enemy[j].pathCompletion < d1)
            {
              this.target = this.main.enemy[j];
              d1 = this.main.enemy[j].pathCompletion;
            }
          }
        }
      }
    }
  }
  
  private void createBullet()
  {
    this.bullet[this.numBullets][0] = (this.x + (int)(Math.cos(this.angle) * 20.0D));
    this.bullet[this.numBullets][1] = (this.y + (int)(Math.sin(this.angle) * 20.0D));
    
    double d = this.angle + (1.0D - Math.random() * 2.0D) * 0.174532925199433D;
    
    this.bullet[this.numBullets][2] = (Math.cos(d) * 20.0D);
    this.bullet[this.numBullets][3] = (Math.sin(d) * 20.0D);
    
    this.numBullets += 1;
  }
  
  private void killBullet(int paramInt)
  {
    this.numBullets -= 1;
    for (int i = paramInt; i < this.numBullets; i++)
    {
      this.bullet[i][0] = this.bullet[(i + 1)][0];
      this.bullet[i][1] = this.bullet[(i + 1)][1];
      this.bullet[i][2] = this.bullet[(i + 1)][2];
      this.bullet[i][3] = this.bullet[(i + 1)][3];
    }
  }
  
  private void bulletHit(int paramInt1, int paramInt2)
  {
    this.main.enemy[paramInt1].health -= this.bulletDamage;
    if (this.main.enemy[paramInt1].health < 1) {
      this.main.killEnemy(paramInt1);
    }
    killBullet(paramInt2);
  }
  
  private void attack()
  {
    if (this.target != null)
    {
      double d1 = Math.sqrt(Math.pow(this.x - this.target.x, 2.0D) + Math.pow(this.y - this.target.y, 2.0D));
      double d2 = this.target.x + this.target.xv * (d1 / 20.0D);
      double d3 = this.target.y + this.target.yv * (d1 / 20.0D);
      
      this.angle = Math.atan2(d3 - this.y, d2 - this.x);
      this.spinSpeed = 45;
      if (this.update == this.fireRate)
      {
        this.update = (-(int)(Math.random() * 3.0D));
        createBullet();
      }
      this.update += 1;
    }
  }
  
  public void run()
  {
    setTarget();
    
    this.spinSpeed -= 1;
    if (this.spinSpeed < 0) {
      this.spinSpeed = 0;
    }
    attack();
    
    this.spin += this.spinSpeed;
    
    runBullets();
  }
  
  private void runBullets()
  {
    for (int i = 0; i < this.numBullets; i++)
    {
      double d1 = this.bullet[i][0];
      double d2 = this.bullet[i][1];
      
      this.bullet[i][0] += this.bullet[i][2];
      this.bullet[i][1] += this.bullet[i][3];
      
      double d3 = this.bullet[i][0];
      double d4 = this.bullet[i][1];
      
      double d5 = this.maxBulletDistance;
      int j = -1;
      for (int k = 0; k < this.main.numEnemys; k++)
      {
        double d7 = Math.sqrt(Math.pow(this.main.enemy[k].x - d3, 2.0D) + Math.pow(this.main.enemy[k].y - d4, 2.0D));
        if (d7 < this.main.enemy[k].radius + 20)
        {
          calculateIntercepts(this.main.enemy[k].x, this.main.enemy[k].y, this.main.enemy[k].radius, d1, d2, d3, d4);
          
          double d8 = Math.sqrt(Math.pow(this.xx1 - this.x, 2.0D) + Math.pow(this.yy1 - this.y, 2.0D));
          double d9 = Math.sqrt(Math.pow(this.xx2 - this.x, 2.0D) + Math.pow(this.yy2 - this.y, 2.0D));
          if (d8 < d5)
          {
            d5 = d8;
            j = k;
          }
          if (d9 < d5)
          {
            d5 = d9;
            j = k;
          }
        }
      }
      double d6 = Math.sqrt(Math.pow(d3 - this.x, 2.0D) + Math.pow(d4 - this.y, 2.0D));
      if (d6 > this.maxBulletDistance) {
        killBullet(i);
      } else if (d5 < d6) {
        bulletHit(j, i);
      }
    }
  }
  
  private void calculateIntercepts(double paramDouble1, double paramDouble2, int paramInt, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6)
  {
    double d1;
    if (paramDouble5 == paramDouble3)
    {
      this.xx1 = (paramDouble3 - paramDouble1);
      this.xx2 = (paramDouble5 - paramDouble1);
      
      d1 = paramInt * paramInt - (paramDouble5 - paramDouble1) * (paramDouble5 - paramDouble1);
      if (d1 > 0.0D)
      {
        this.yy1 = Math.sqrt(d1);
        this.yy2 = (-this.yy1 + paramDouble2);
        this.yy1 += paramDouble2;
      }
      else
      {
        this.xx1 = (-this.maxBulletDistance);
        this.xx2 = (-this.maxBulletDistance);
      }
    }
    else
    {
      d1 = (paramDouble6 - paramDouble2 - (paramDouble4 - paramDouble2)) / (paramDouble5 - paramDouble1 - (paramDouble3 - paramDouble1));
      double d2 = paramDouble4 - paramDouble2 - d1 * (paramDouble3 - paramDouble1);
      
      double d3 = Math.pow(2.0D * d1 * d2, 2.0D) - 4.0D * (d1 * d1 + 1.0D) * (d2 * d2 - paramInt * paramInt);
      if (d3 > 0.0D)
      {
        double d4 = 2.0D * (d1 * d1 + 1.0D);
        
        double d5 = -(2.0D * d1 * d2);
        double d6 = Math.sqrt(d3);
        double d7 = d5 + d6;
        double d8 = d5 - d6;
        
        this.xx1 = (d7 / d4);
        this.xx2 = (d8 / d4);
        
        this.yy1 = (d1 * this.xx1 + d2 + paramDouble2);
        this.yy2 = (d1 * this.xx2 + d2 + paramDouble2);
        
        this.xx1 += paramDouble1;
        this.xx2 += paramDouble1;
      }
      else
      {
        this.xx1 = (-this.maxBulletDistance);
        this.xx2 = (-this.maxBulletDistance);
      }
    }
  }
  
  public void drawAttackRange(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.attackRangeColor);
    paramGraphics.fillOval(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2);
  }
  
  public void draw(Graphics paramGraphics)
  {
    drawBullets(paramGraphics);
    
    paramGraphics.setColor(this.towerColor);
    paramGraphics.fillOval(this.x - 8, this.y - 8, 16, 16);
    
    paramGraphics.setColor(this.gunColor);
    
    int i = (int)(this.x + Math.cos(this.angle) * 20.0D + Math.cos(this.angle + 90.0D) * 5.0D);
    int j = (int)(this.y + Math.sin(this.angle) * 20.0D + Math.sin(this.angle + 90.0D) * 5.0D);
    int k = (int)(this.x + Math.cos(this.angle) * 20.0D + Math.cos(this.angle - 90.0D) * 5.0D);
    int m = (int)(this.y + Math.sin(this.angle) * 20.0D + Math.sin(this.angle - 90.0D) * 5.0D);
    int n = (int)(this.x - Math.cos(this.angle) * 5.0D + Math.cos(this.angle - 90.0D) * 5.0D);
    int i1 = (int)(this.y - Math.sin(this.angle) * 5.0D + Math.sin(this.angle - 90.0D) * 5.0D);
    int i2 = (int)(this.x - Math.cos(this.angle) * 5.0D + Math.cos(this.angle + 90.0D) * 5.0D);
    int i3 = (int)(this.y - Math.sin(this.angle) * 5.0D + Math.sin(this.angle + 90.0D) * 5.0D);
    
    paramGraphics.setColor(this.gunColor);
    
    Polygon localPolygon = new Polygon();
    localPolygon.addPoint((int)(this.x + Math.cos(this.angle) * 20.0D + Math.cos(this.angle + 90.0D) * 5.0D), (int)(this.y + Math.sin(this.angle) * 20.0D + Math.sin(this.angle + 90.0D) * 5.0D));
    localPolygon.addPoint((int)(this.x + Math.cos(this.angle) * 20.0D + Math.cos(this.angle - 90.0D) * 5.0D), (int)(this.y + Math.sin(this.angle) * 20.0D + Math.sin(this.angle - 90.0D) * 5.0D));
    localPolygon.addPoint((int)(this.x - Math.cos(this.angle) * 5.0D + Math.cos(this.angle - 90.0D) * 5.0D), (int)(this.y - Math.sin(this.angle) * 5.0D + Math.sin(this.angle - 90.0D) * 5.0D));
    localPolygon.addPoint((int)(this.x - Math.cos(this.angle) * 5.0D + Math.cos(this.angle + 90.0D) * 5.0D), (int)(this.y - Math.sin(this.angle) * 5.0D + Math.sin(this.angle + 90.0D) * 5.0D));
    paramGraphics.fillPolygon(localPolygon);
  }
  
  private void drawBullets(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.bulletColor);
    for (int i = 0; i < this.numBullets; i++) {
      paramGraphics.drawLine((int)this.bullet[i][0], (int)this.bullet[i][1], (int)(this.bullet[i][0] - this.bullet[i][2] / 2.0D), (int)(this.bullet[i][1] - this.bullet[i][3] / 2.0D));
    }
  }
}
