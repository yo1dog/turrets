import java.applet.Applet;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class Main
  extends Applet
  implements Runnable
{
  private final int height = 400;
  private final int width = 400;
  private final int totalEnemys = 20;
  private final GUI gui = new GUI(400, 400, this);
  private final byte ELECTRIC = 0;
  private final byte GUN = 1;
  private final byte LASER = 2;
  private final byte FIRE = 3;
  private final byte AIRSTRIKE = 4;
  private int[] numTower = new int[5];
  private final Thread th = new Thread(this);
  private final Color backgroundColor = new Color(40, 40, 40);
  private int timer;
  public int numEnemys;
  public int mx;
  public int my;
  private boolean pause;
  public Path path;
  public Enemy[] enemy;
  private ElectricTower[] electricTower;
  private GunTower[] gunTower;
  private LaserTower[] laserTower;
  private FireTower[] fireTower;
  private AirstrikeTower[] airstrikeTower;
  private Image planeImage;
  private Graphics dbg;
  private Image dbImage;
  
  public void init()
  {
    setBackground(Color.black);
    this.timer = 0;
    this.pause = true;
    
    try
	{
		getImages();
	}
	catch (IOException e)
	{
		e.printStackTrace();
	}
    

    int[][] arrayOfInt = new int[6][2];
    arrayOfInt[0][0] = -20;arrayOfInt[0][1] = 40;
    arrayOfInt[1][0] = 350;arrayOfInt[1][1] = 40;
    arrayOfInt[2][0] = 350;arrayOfInt[2][1] = 170;
    arrayOfInt[3][0] = 40;arrayOfInt[3][1] = 170;
    arrayOfInt[4][0] = 40;arrayOfInt[4][1] = 300;
    arrayOfInt[5][0] = 420;arrayOfInt[5][1] = 300;
    
    this.path = new Path(6, arrayOfInt, 30, Color.red);
    

    this.enemy = new Enemy[20];
    this.numEnemys = 1;
    this.enemy[0] = new Enemy(2.0D, 10, 300, Color.green, this.path, 0);
  }
  
  private void getImages() throws IOException
  { 
    MediaTracker localMediaTracker = new MediaTracker(this);

    this.planeImage = ImageIO.read(getClass().getResourceAsStream("Plane.gif"));
    localMediaTracker.addImage(this.planeImage, 1);
    try
    {
      localMediaTracker.waitForAll();
    }
    catch (Exception localException) {}
  }
  
  public void start()
  {
    this.th.start();
  }
  
  public void stop()
  {
    this.th.stop();
  }
  
  public boolean mouseMove(Event paramEvent, int paramInt1, int paramInt2)
  {
    this.mx = paramInt1;
    this.my = paramInt2;
    
    return true;
  }
  
  public void killEnemy(int paramInt)
  {
    this.enemy[paramInt].dead = true;
    for (int i = paramInt; i < this.numEnemys - 1; i++)
    {
      this.enemy[i] = this.enemy[(i + 1)];
      this.enemy[i].ID -= 1;
    }
    this.enemy[(this.numEnemys - 1)] = null;
    this.numEnemys -= 1;
  }
  
  public void addTower(int paramInt1, int paramInt2, byte paramByte)
  {
    Object[] localObject;
    int i;
    if (paramByte == 0)
    {
      localObject = this.electricTower;
      this.electricTower = new ElectricTower[this.numTower[0] + 1];
      for (i = 0; i < this.numTower[0]; i++) {
        this.electricTower[i] = (ElectricTower) localObject[i];
      }
      this.electricTower[this.numTower[0]] = new ElectricTower(paramInt1, paramInt2, this);
      this.numTower[0] += 1;
    }
    else if (paramByte == 1)
    {
      localObject = this.gunTower;
      this.gunTower = new GunTower[this.numTower[1] + 1];
      for (i = 0; i < this.numTower[1]; i++) {
        this.gunTower[i] = (GunTower) localObject[i];
      }
      this.gunTower[this.numTower[1]] = new GunTower(paramInt1, paramInt2, 400, 400, this);
      this.numTower[1] += 1;
    }
    else if (paramByte == 2)
    {
      localObject = this.laserTower;
      this.laserTower = new LaserTower[this.numTower[2] + 1];
      for (i = 0; i < this.numTower[2]; i++) {
        this.laserTower[i] = (LaserTower) localObject[i];
      }
      this.laserTower[this.numTower[2]] = new LaserTower(paramInt1, paramInt2, this);
      this.numTower[2] += 1;
    }
    else if (paramByte == 3)
    {
      localObject = this.fireTower;
      this.fireTower = new FireTower[this.numTower[3] + 1];
      for (i = 0; i < this.numTower[3]; i++) {
        this.fireTower[i] = (FireTower) localObject[i];
      }
      this.fireTower[this.numTower[3]] = new FireTower(paramInt1, paramInt2, this);
      this.numTower[3] += 1;
    }
    else if (paramByte == 4)
    {
      localObject = this.airstrikeTower;
      this.airstrikeTower = new AirstrikeTower[this.numTower[4] + 1];
      for (i = 0; i < this.numTower[4]; i++) {
        this.airstrikeTower[i] = (AirstrikeTower) localObject[i];
      }
      this.airstrikeTower[this.numTower[4]] = new AirstrikeTower(paramInt1, paramInt2, this.backgroundColor, this.planeImage, this.path, this);
      this.numTower[4] += 1;
    }
  }
  
  public boolean mouseDown(Event paramEvent, int paramInt1, int paramInt2)
  {
    this.gui.mouseDown(paramInt1, paramInt2);
    
    return true;
  }
  
  public boolean keyDown(Event paramEvent, int paramInt)
  {
    if (paramInt == 32)
    {
      if (this.pause) {
        this.pause = false;
      } else {
        this.pause = true;
      }
    }
    else {
      this.gui.keyDown(paramInt);
    }
    return true;
  }
  
  public void run()
  {
    Thread.currentThread().setPriority(10);
    for (;;)
    {
      if (!this.pause)
      {
        this.timer += 1;
        if (this.timer % 30 == 0) {
          if (this.numEnemys < 20)
          {
            this.enemy[this.numEnemys] = new Enemy(Math.random() * 2.0D + 1.0D, 10, 300, Color.green, this.path, this.numEnemys);
            this.numEnemys += 1;
          }
        }
        for (int i = 0; i < this.numEnemys; i++) {
          this.enemy[i].run();
        }
        for (int i = 0; i < this.numTower[0]; i++) {
          this.electricTower[i].run();
        }
        for (int i = 0; i < this.numTower[1]; i++) {
          this.gunTower[i].run();
        }
        for (int i = 0; i < this.numTower[2]; i++) {
          this.laserTower[i].run();
        }
        for (int i = 0; i < this.numTower[3]; i++) {
          this.fireTower[i].run();
        }
        for (int i = 0; i < this.numTower[4]; i++) {
          this.airstrikeTower[i].run();
        }
      }
      repaint();
      try
      {
        Thread.sleep(16L);
      }
      catch (InterruptedException localInterruptedException) {}
      Thread.currentThread().setPriority(10);
    }
  }
  
  public void paint(Graphics paramGraphics)
  {
    Graphics2D localGraphics2D = (Graphics2D)paramGraphics;
    RenderingHints localRenderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    localGraphics2D.setRenderingHints(localRenderingHints);
    
    this.path.draw(paramGraphics);
    for (int i = 0; i < this.numEnemys; i++) {
      this.enemy[i].draw(paramGraphics);
    }
    for (int i = 0; i < this.numTower[0]; i++) {
      this.electricTower[i].draw(paramGraphics, localGraphics2D);
    }
    for (int i = 0; i < this.numTower[1]; i++) {
      this.gunTower[i].draw(paramGraphics);
    }
    for (int i = 0; i < this.numTower[2]; i++) {
      this.laserTower[i].draw(paramGraphics);
    }
    for (int i = 0; i < this.numTower[3]; i++) {
      this.fireTower[i].draw(paramGraphics);
    }
    for (int i = 0; i < this.numTower[4]; i++) {
      this.airstrikeTower[i].draw(paramGraphics);
    }
    this.gui.draw(paramGraphics, localGraphics2D);
  }
  
  public void update(Graphics paramGraphics)
  {
    if (this.dbImage == null)
    {
      this.dbImage = createImage(getSize().width, getSize().height);
      this.dbg = this.dbImage.getGraphics();
    }
    this.dbg.setColor(this.backgroundColor);
    this.dbg.fillRect(0, 0, 400, 400);
    
    this.dbg.setColor(getForeground());
    paint(this.dbg);
    
    paramGraphics.drawImage(this.dbImage, 0, 0, this);
  }
}
