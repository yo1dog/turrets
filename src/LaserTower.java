import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class LaserTower
{
  private final int x;
  private final int y;
  private final Color towerColor;
  private final int cR;
  private final int cG;
  private final int cB;
  private final Color gunColor;
  private final Color attackRangeColor = new Color(255, 255, 255, 50);
  private double angle;
  private byte update;
  private final int maxAttackDistance = 150;
  private int attackDistance = 150;
  private int laserLength = this.attackDistance;
  private int laserDamage = 65;
  private byte fireRate = 40;
  private final int laserFadeTime = 20;
  private int laserFade;
  private boolean laserActive;
  public Enemy target;
  private boolean first = false;
  private boolean trace = false;
  private double x1;
  private double y1;
  private double x2;
  private double y2;
  private double xx;
  private double yy;
  private final Main main;
  
  public LaserTower(int paramInt1, int paramInt2, Main paramMain)
  {
    this.x = paramInt1;
    this.y = paramInt2;
    this.main = paramMain;
    
    this.angle = 0.0D;
    this.towerColor = Color.gray;
    this.gunColor = Color.yellow;
    this.cR = 0;this.cG = 183;this.cB = 239;
    this.update = this.fireRate;
    this.laserActive = false;
    this.laserFade = 0;
  }
  
  public void setTarget()
  {
    if (this.trace)
    {
      int i = 0;
      double d2;
      if ((this.target == null) || (this.target.dead == true))
      {
        i = 1;
      }
      else
      {
        d2 = Math.sqrt(Math.pow(this.target.x - this.x, 2.0D) + Math.pow(this.target.y - this.y, 2.0D));
        if ((d2 > this.attackDistance + this.target.radius) || (this.target.health < 1)) {
          i = 1;
        }
      }
      if (i != 0)
      {
        this.target = null;
        int k;
        double d4;
        if (this.first)
        {
          d2 = 0.0D;
          for (k = 0; k < this.main.numEnemys; k++)
          {
            d4 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
            if (d4 < this.attackDistance + this.main.enemy[k].radius) {
              if (this.main.enemy[k].pathCompletion > d2)
              {
                this.target = this.main.enemy[k];
                d2 = this.main.enemy[k].pathCompletion;
              }
            }
          }
        }
        else
        {
          d2 = 1.0D;
          for (k = 0; k < this.main.numEnemys; k++)
          {
            d4 = Math.sqrt(Math.pow(this.main.enemy[k].x - this.x, 2.0D) + Math.pow(this.main.enemy[k].y - this.y, 2.0D));
            if (d4 < this.attackDistance + this.main.enemy[k].radius) {
              if (this.main.enemy[k].pathCompletion < d2)
              {
                this.target = this.main.enemy[k];
                d2 = this.main.enemy[k].pathCompletion;
              }
            }
          }
        }
      }
    }
    else
    {
      this.target = null;
      double d1;
      int j;
      double d3;
      if (this.first)
      {
        d1 = 0.0D;
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d3 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d3 < this.attackDistance + this.main.enemy[j].radius) {
            if (this.main.enemy[j].pathCompletion > d1)
            {
              this.target = this.main.enemy[j];
              d1 = this.main.enemy[j].pathCompletion;
            }
          }
        }
      }
      else
      {
        d1 = 1.0D;
        for (j = 0; j < this.main.numEnemys; j++)
        {
          d3 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
          if (d3 < this.attackDistance + this.main.enemy[j].radius) {
            if (this.main.enemy[j].pathCompletion < d1)
            {
              this.target = this.main.enemy[j];
              d1 = this.main.enemy[j].pathCompletion;
            }
          }
        }
      }
    }
  }
  
  private boolean laserHit(int paramInt)
  {
    this.main.enemy[paramInt].health -= this.laserDamage;
    if (this.main.enemy[paramInt].health < 1)
    {
      this.main.killEnemy(paramInt);
      return true;
    }
    return false;
  }
  
  private void attack()
  {
    int i = 0;
    
    int[] arrayOfInt = new int[this.main.numEnemys];
    for (int j = 0; j < this.main.numEnemys; j++)
    {
      double d1 = Math.sqrt(Math.pow(this.main.enemy[j].x - this.x, 2.0D) + Math.pow(this.main.enemy[j].y - this.y, 2.0D));
      if (d1 < this.attackDistance + this.main.enemy[j].radius) {
        if (this.x2 == this.x1)
        {
          if (Math.abs(this.x2 - this.main.enemy[j].x) < this.main.enemy[j].radius)
          {
            arrayOfInt[i] = j;
            i++;
          }
        }
        else if (this.y2 == this.y1)
        {
          if (Math.abs(this.y2 - this.main.enemy[j].y) < this.main.enemy[j].radius)
          {
            arrayOfInt[i] = j;
            i++;
          }
        }
        else
        {
          double d2 = (this.y2 - this.y1) / (this.x2 - this.x1);
          double d3 = this.y1 - d2 * this.x1;
          double d4 = -1.0D / d2;
          
          this.xx = ((this.main.enemy[j].y - d4 * this.main.enemy[j].x - d3) / (d2 - d4));
          this.yy = (d2 * this.xx + d3);
          
          double d5 = Math.sqrt((this.main.enemy[j].y - this.yy) * (this.main.enemy[j].y - this.yy) + (this.main.enemy[j].x - this.xx) * (this.main.enemy[j].x - this.xx));
          if (d5 < this.main.enemy[j].radius)
          {
            arrayOfInt[i] = j;
            i++;
          }
        }
      }
    }
    int j = 0;
    for (int k = 0; j < i; j++) {
      if (laserHit(arrayOfInt[j] - k)) {
        k++;
      }
    }
  }
  
  public void run()
  {
    if (this.laserFade > 0)
    {
      this.laserFade -= 1;
    }
    else
    {
      setTarget();
      if (this.target != null)
      {
        this.angle = Math.atan2(this.target.y - this.y, this.target.x - this.x);
        
        this.x1 = (this.x + Math.cos(this.angle) * 20.0D);
        this.y1 = (this.y + Math.sin(this.angle) * 20.0D);
        this.x2 = (this.x + Math.cos(this.angle) * this.attackDistance);
        this.y2 = (this.y + Math.sin(this.angle) * this.attackDistance);
      }
      if (this.update == this.fireRate)
      {
        if (this.target != null)
        {
          attack();
          this.laserFade = 20;
          this.update = 0;
          
          this.target = null;
        }
      }
      else {
        this.update = ((byte)(this.update + 1));
      }
    }
  }
  
  public void drawAttackRange(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.attackRangeColor);
    paramGraphics.fillOval(this.x - this.attackDistance, this.y - this.attackDistance, this.attackDistance * 2, this.attackDistance * 2);
  }
  
  public void draw(Graphics paramGraphics)
  {
    paramGraphics.setColor(this.towerColor);
    paramGraphics.fillOval(this.x - 5, this.y - 5, 10, 10);
    
    paramGraphics.setColor(this.gunColor);
    
    Polygon localPolygon = new Polygon();
    localPolygon.addPoint((int)(this.x + Math.cos(this.angle) * 20.0D + Math.cos(this.angle + 90.0D) * 2.0D), (int)(this.y + Math.sin(this.angle) * 20.0D + Math.sin(this.angle + 90.0D) * 2.0D));
    localPolygon.addPoint((int)(this.x + Math.cos(this.angle) * 20.0D + Math.cos(this.angle - 90.0D) * 2.0D), (int)(this.y + Math.sin(this.angle) * 20.0D + Math.sin(this.angle - 90.0D) * 2.0D));
    localPolygon.addPoint((int)(this.x - Math.cos(this.angle) * 2.0D + Math.cos(this.angle - 90.0D) * 2.0D), (int)(this.y - Math.sin(this.angle) * 2.0D + Math.sin(this.angle - 90.0D) * 2.0D));
    localPolygon.addPoint((int)(this.x - Math.cos(this.angle) * 2.0D + Math.cos(this.angle + 90.0D) * 2.0D), (int)(this.y - Math.sin(this.angle) * 2.0D + Math.sin(this.angle + 90.0D) * 2.0D));
    paramGraphics.fillPolygon(localPolygon);
    
    paramGraphics.setColor(new Color(this.cR, this.cG, this.cB, (int)(this.laserFade / 20.0D * 255.0D)));
    paramGraphics.drawLine((int)this.x1, (int)this.y1, (int)this.x2, (int)this.y2);
    
    paramGraphics.setColor(new Color(this.cR, this.cG, this.cB, (int)(this.laserFade / 60.0D * 255.0D)));
    paramGraphics.drawLine((int)(this.x1 + Math.cos(this.angle + 90.0D)), (int)(this.y1 + Math.sin(this.angle + 90.0D)), (int)(this.x2 + Math.cos(this.angle + 90.0D)), (int)(this.y2 + Math.sin(this.angle + 90.0D)));
    paramGraphics.drawLine((int)(this.x1 + Math.cos(this.angle - 90.0D)), (int)(this.y1 + Math.sin(this.angle - 90.0D)), (int)(this.x2 + Math.cos(this.angle - 90.0D)), (int)(this.y2 + Math.sin(this.angle - 90.0D)));
  }
}
